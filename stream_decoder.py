def pack_word(bits):
    size = min(len(bits), 32)
    return sum(1 << (size - 1 - i) for i in range(size) if bits[i])


def unpack_word(word):
    return [(word >> i) & 1 for i in range(31, -1, -1)]


def unpack_array(data):
    return [bit for word in data for bit in unpack_word(word)]


def decode_substitution(bits):
    result = []
    i = 0
    while i + 1 < len(bits):
        if bits[i : i + 2] == [0, 0]:
            result += [0, 1, 0, 1]
            i += 2
        elif bits[i : i + 2] == [0, 1]:
            result += [0, 1]
            i += 1
        else:
            result += bits[i : i + 2]
            i += 2
    return result


def decode_hitmap(bits):
    code = decode_substitution(bits)
    result = []
    s2_offset = 2
    for i in range(2):
        if code[i]:
            n_s3 = sum(code[s2_offset : s2_offset + 2])
            s3_offset = s2_offset + 2
            s4_offset = s3_offset + 2 * n_s3
            for j in range(2):
                if code[s2_offset + j]:
                    for k in range(2):
                        if code[s3_offset + k]:
                            for l in range(2):
                                if code[s4_offset + l]:
                                    result.append((i, 4 * j + 2 * k + l))
                            s4_offset += 2
                    s3_offset += 2
            s2_offset = s4_offset
    size = s2_offset
    for i in range(0, s2_offset, 2):
        if code[i : i + 2] == [0, 1]:
            size -= 1
    # print(size)

    return size, result


def indent(str, level = 1):
    return "\n".join('\t' * level + line for line in str.split('\n'))

class Node():
    def __init__(self, **kwargs):
        self.attributes = kwargs.keys()
        for key in kwargs:
            setattr(self, key, kwargs[key])
        self.items = []

    def __getitem__(self, i):
        return self.items[i]

    def append(self, item):
        self.items.append(item)

    def __len__(self):
        return len(self.items)

    def __str__(self):
        attributes_str = ",\n".join(["%s: %s" % (attr, getattr(self, attr)) for attr in self.attributes])
        if len(self.items):
            return "{\n%s,\n\titems:\n\t[\n%s\n\t]\n}" % (indent(attributes_str, 1), indent(",\n".join(map(str, self.items)), 2))
        else:
            return "{\n%s\n}" % (indent(attributes_str, 1),)

    def __repr__(self):
        return str(self)


## Tokens

def end_of_stream(bits, stream_events):
    if len(bits) < 6 or not any(bits[:6]):
        return 6, []
    else:
        return 0, []


def tag(bits, events):
    if len(bits) >= 8:
        events.append(Node(tag=pack_word(bits[:8])))
        return 8, [internal_tag, ccol, end_of_stream]
    else:
        return 0, []


def internal_tag(bits, events):
    if bits[:3] == [1,1,1]:
        return 3, [tag]
    else:
        return 0, []


def ccol(bits, events):
    core_col = pack_word(bits[:6])
    if core_col > 1 and core_col < 56:
        events[-1].append(Node(ccol=core_col))
        return 6, [qrow]
    else:
        return 0, []


def qrow(bits, events):
    core_col = events[-1][-1]
    if bits[1]:
        if len(core_col) > 0:
            quarter_row = core_col[-1].qrow + 1
            offset = 2
        else:
            return 0, []
    else:
        quarter_row = pack_word(bits[2:10])
        offset = 10
    hitmap_size, hitmap = decode_hitmap(bits[offset:])
    offset += hitmap_size
    new_row = Node(qrow=quarter_row, hitmap=hitmap)
    for hit in hitmap:
        if hit:
            new_row.append(Node(tot=pack_word(bits[offset : offset + 4])))
            offset += 4
    core_col.append(new_row)
    if not bits[0]:
        return offset, [qrow]
    else:
        return offset, [ccol, internal_tag, end_of_stream]
        

def decode_stream(data):
    bits = unpack_array(data)
    events = []
    tokens = [tag]
    offset = 0
    while len(tokens) > 0:
        for token in tokens:
            size, new_tokens = token(bits[offset:], events)
            if size > 0:
                tokens = new_tokens
                offset += size
                break
    return events
